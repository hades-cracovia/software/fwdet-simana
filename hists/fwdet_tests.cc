#include "fwdet_tests.h"

#include "hgeantfwdet.h"
#include "fwdetdef.h"
#include "hfwdetstrawcalsim.h"
#include "hfwdetstrawgeompar.h"
#include "hfwdetrpccalsim.h"
#include "hfwdetrpchit.h"
#include "hfwdetcand.h"
#include "hfwdetcandsim.h"
#include "hparasciifileio.h"
#include "hades.h"
#include "hspectrometer.h"

#include "TCanvas.h"
#include "TColor.h"
#include "TH2I.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TGaxis.h"
#include "TFile.h"

#include <cstdlib>

#define PR(x) std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

using namespace std;

const Float_t avg_beam_rate = 400e3; // 400 kHz

HGeomVector calcPrimVertex_Track_Mother(const std::vector<HParticleCand *>cands, const HGeomVector & DecayVertex, const HGeomVector & dirMother, int trackA_num, int trackB_num);
HGeomVector calcPrimVertex_Track_Mother(const std::vector<HParticleCand *>cands, const HGeomVector & beamVector, const HGeomVector & DecayVertex, const HGeomVector & dirMother, int trackA_num, int trackB_num);

void format_h(TH1 * h) {
//        h->SetLineStyle(9);
    h->SetTitle("");
    h->GetXaxis()->SetLabelSize(0.055);
    h->GetXaxis()->SetTitleSize(0.055);
    h->GetXaxis()->SetTitleOffset(1.20);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetLabelSize(0.055);
    h->GetYaxis()->SetTitleSize(0.055);
    h->GetYaxis()->SetTitleOffset(1.30);
    gPad->SetLeftMargin(0.14);
    gPad->SetTopMargin(0.05);
    gPad->SetRightMargin(0.05);
    gPad->SetBottomMargin(0.14);
}

void format_h_1_1_2_1(TH1 * h) {
//        h->SetLineStyle(9);
    h->SetTitle("");
    h->GetXaxis()->SetLabelSize(0.065);
    h->GetXaxis()->SetTitleSize(0.065);
    h->GetXaxis()->SetTitleOffset(0.95);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetLabelSize(0.065);
    h->GetYaxis()->SetTitleSize(0.065);
    h->GetYaxis()->SetTitleOffset(1.05);
    gPad->SetLeftMargin(0.14);
    gPad->SetTopMargin(0.05);
    gPad->SetRightMargin(0.05);
    gPad->SetBottomMargin(0.14);
}

const Int_t beach1(TColor::GetColor("#fe4a49"));
const Int_t beach2(TColor::GetColor("#2ab7ca"));
const Int_t beach3(TColor::GetColor("#fed766"));
const Int_t beach4(TColor::GetColor("#e6e6ea"));
const Int_t beach5(TColor::GetColor("#f4f4f8"));

const Int_t metro1(TColor::GetColor("#d11141"));
const Int_t metro2(TColor::GetColor("#00b159"));
const Int_t metro3(TColor::GetColor("#00aedb"));
const Int_t metro4(TColor::GetColor("#f37735"));
const Int_t metro5(TColor::GetColor("#ffc425"));

Double_t ChiSquareDistr(Double_t *x,Double_t *par)
{
    // Chisquare density distribution for nrFree degrees of freedom

    Double_t nrFree = par[0];
    Double_t scale = par[1];
    Double_t sup = par[2];
    Double_t chi2 = x[0];

    if (chi2 > 0) {
        Double_t lambda = nrFree/2.;
        Double_t norm = TMath::Gamma(lambda) * TMath::Power(2.,lambda);
        return scale*TMath::Power(chi2*sup,lambda-1) * TMath::Exp(-0.5*chi2*sup)/norm;
    } else
    return 0.0;
}

struct TrackTracker
{
    unsigned int layer[8];
    TrackTracker() { clear(); }
    void clear() { for (int i = 0; i < 8; ++i) layer[i] = 0; }
    bool is_good() {
        for (int i = 0; i < 8; ++i) { if (layer[i] == 0) return false; }
        return true;
    }
    int count() {
        int cnt = 0;
        for (int i = 0; i < 8; ++i) { if (layer[i] > 0) ++cnt; }
        return cnt;
    }
};

void clean(std::vector<Int_t> & vec)
{
    sort( vec.begin(), vec.end() );
    vec.erase( unique( vec.begin(), vec.end() ), vec.end() );
}

Int_t fwdet_tests(HLoop * loop, const AnaParameters & anapars)
{
    if (!loop->setInput(""))
    {                                                    // reading file structure
        std::cerr << "READBACK: ERROR : cannot read input !" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    TStopwatch timer;
    timer.Reset();
    timer.Start();

    //////////////////////////////////////////////////////////////////////////////
    //      Fast tree builder for creating of ntuples                            //
    //////////////////////////////////////////////////////////////////////////////

//     Hades *myHades = new Hades;
//     myHades->setTreeBufferSize(8000);
//     HSpectrometer* spec = gHades->getSetup();

    TString asciiParFile = "fwdetparams_digi.txt";
    HRuntimeDb *rtdb = HRuntimeDb::instance();//myHades -> getRuntimeDb();

    if (!asciiParFile.IsNull()) {
        HParAsciiFileIo *input1=new HParAsciiFileIo;
        input1->open((Text_t*)asciiParFile.Data(),"in");
        if (!input1->check())
        {
            std::cerr << "Param file " << asciiParFile << " not open!" <<std::endl;
            abort();
        }
        rtdb->setFirstInput(input1);
    }


    // create the parameter container
    HFwDetStrawGeomPar* pStrawGeomPar = (HFwDetStrawGeomPar *)gHades->getRuntimeDb()->getContainer("FwDetStrawGeomPar");
    if (!pStrawGeomPar)
    {
        Error("HFwDetStrawDigitizer::init()","Parameter container for geometry not created");
        return kFALSE;
    }
    rtdb->initContainers(1);
    rtdb->print();

    pStrawGeomPar->printParams();

    Float_t sina[FWDET_STRAW_MAX_MODULES][FWDET_STRAW_MAX_LAYERS];
    Float_t cosa[FWDET_STRAW_MAX_MODULES][FWDET_STRAW_MAX_LAYERS];

    for (Int_t m = 0; m < FWDET_STRAW_MAX_MODULES; ++m)
        for (Int_t l = 0; l < FWDET_STRAW_MAX_LAYERS; ++l)
        {
            Float_t a = pStrawGeomPar->getLayerRotation(m, l) * TMath::DegToRad();
            cosa[m][l] = TMath::Cos(a);
            sina[m][l] = TMath::Sin(a);
        }

    loop->printCategories();    // print all categories found in input + status
    //     loop->printChain();            // print all files in the chain
    //     loop->Print();

    //     HEventHeader * header = loop->getEventHeader();

    HCategory * fCatGeantKine = nullptr;
    fCatGeantKine = HCategoryManager::getCategory(catGeantKine, kTRUE, "catGeantKine");

    if (!fCatGeantKine)
    {
        cout << "No catGeantKine!" << endl;
        exit(EXIT_FAILURE);  // do you want a brute force exit ?
    }

    HCategory * fCatGeantFwDet = nullptr;
    fCatGeantFwDet = HCategoryManager::getCategory(catFwDetGeantRaw, kTRUE, "catGeantFwDet");

    if (!fCatGeantFwDet)
    {
        cout << "No catGeantFwDet!" << endl;
        exit(EXIT_FAILURE);  // do you want a brute force exit ?
    }

    HCategory * fFwDetStrawCal = nullptr;
    fFwDetStrawCal = HCategoryManager::getCategory(catFwDetStrawCal, kTRUE, "catFwDetStrawCalSim");

    if (!fFwDetStrawCal)
    {
        cout << "No catFwDetStrawCal!" << endl;
//         exit(EXIT_FAILURE);  // do you want a brute force exit ?
    }

    HCategory * fFwDetCand = nullptr;
    fFwDetCand = HCategoryManager::getCategory(catFwDetCand, kTRUE, "catFwDetCand");

    if (!fFwDetCand)
    {
        cout << "No catFwDetCand!" << endl;
//         exit(EXIT_FAILURE);  // do you want a brute force exit ?
    }

    HCategory * fFwDetRpcHit = nullptr;
    fFwDetRpcHit = HCategoryManager::getCategory(catFwDetRpcHit, kTRUE, "catFwDetRpcHit");

    if (!fFwDetRpcHit)
    {
        cout << "No catFwDetRpcHit!" << endl;
//         exit(EXIT_FAILURE);  // do you want a brute force exit ?
    }

    HCategory * fFwDetRpcCal = nullptr;
    fFwDetRpcCal = HCategoryManager::getCategory(catFwDetRpcCal, kTRUE, "catFwDetRpcCalSim");

    if (!fFwDetRpcCal)
    {
        cout << "No catFwDetRpcCal!" << endl;
//         exit(EXIT_FAILURE);  // do you want a brute force exit ?
    }

    //

    Int_t entries = loop->getEntries();
    int limit_sta = anapars.start;
    int limit_sto = 0;

    if (anapars.length > 0)
        limit_sto = limit_sta + anapars.length;
    else if (anapars.events >= 0)
        limit_sto = limit_sta + anapars.events;
    else limit_sto = entries;

    //     //setting numbers of events regarding the input number of events by the user
    if ( limit_sto > entries )
        limit_sto = entries;

    //     // specify output file
    TFile * output_file = TFile::Open(anapars.outfile, "RECREATE");
    output_file->cd();
    //
    cout << "NEW ROOT TREE " << endl;
    //

    const size_t straw_mods = 2;
    const size_t straw_layers = 4;

    const size_t rpc_mods = 2;
    const size_t rpc_layers = 2;
    const size_t rpc_strips = 30;
    char buff1[200];
    char buff2[200];

    TH1I * h_tr_mult = new TH1I("h_tr_mult", "Tracks mult;multiplicity", 100, 0, 100);

    TH1I * h_tr_sts_mult = new TH1I("h_tr_sts_mult", "STS Tracks mult;multiplicity", 50, 0, 50);
    TH2I * h_tr_sts_mult_vs_hits = new TH2I("h_tr_sts_mult_vs_hits", "STS Tracks vs hits;tracks;hits", 50, 0, 50, 300, 0, 300);
    TH1I * h_tr_sts_num = new TH1I("h_tr_sts_num", "STS Tracks num;number", 1, 0, 1);
    TH1I * h_tr_sts_planes = new TH1I("h_tr_sts_planes", "STS Planes used by track;used planes;count", 8, 1, 9);
    TH1I * h_vec_sts_mult = new TH1I("h_vec_sts_mult", "Vectors mult;multiplicity", 15, 0, 15);
    TH1I * h_vec_sts_num = new TH1I("h_vec_sts_num", "Tracks num;number", 1, 0, 1);
    TH1I * h_vec_sts_good_num = new TH1I("h_vec_sts_good_num", "Good tracks num;number", 1, 0, 1);

    TH1I * h_tr_rpc_mult = new TH1I("h_tr_rpc_mult", "RPC Tracks mult;multiplicity", 50, 0, 50);

    TH2I * h_tr_sts_vs_rpc_mult = new TH2I("h_tr_sts_vs_rpc_mult", "STS vs RPC Tracks mult;Unique tracks in STS multiplicity;Unique tracks in RPC multiplicity", 50, 0, 50, 50, 0, 50);

    TCanvas * c_tr_fd_mult = new TCanvas("c_tr_fd_mult", "STS and RPC mult", 1200, 600);
    c_tr_fd_mult->Divide(2, 1);

    TH2I * h_tr_vec_sts_mult = new TH2I("h_tr_vec_sts_mult", "Vectors mult;tracks;vectors", 10, 0, 10, 10, 0, 10);
    TH2I * h_tr_good_vec_sts_mult = new TH2I("h_tr_good_vec_sts_mult", "Vectors mult;good tracks;vectors", 10, 0, 10, 10, 0, 10);
    TH2I * h_tr_good_vec_sts_good_mult = new TH2I("h_tr_good_vec_sts_good_mult", "Good vectors mult;good tracks;vectors", 10, 0, 10, 10, 0, 10);

    TH1D * h_vec_gea_cors = new TH1D("h_vec_gea_cors", "Geant correlated tracks;no. of correlated tracks;counts", 8, 0, 8);
    TH1I * h_vec_chi2 = new TH1I("h_vec_chi2", ";#chi^{2}/ndf;counts", 250, 0, 5);
    TH1I * h_vec_chi2_fixed = new TH1I("h_vec_chi2_fixed", "Chi2;chi2/ndf;counts", 250, 0, 5);
    TH1I * h_vec_chi2_good = new TH1I("h_vec_chi2_good", "Chi2/ndf;counts", 250, 0, 5);
    TH1I * h_vec_chi2_notofrec = new TH1I("h_vec_chi2_notofrec", "Chi2/ndf;counts", 2500, 0, 50);

    TH1I * h_vec_dt_avg = new TH1I("h_vec_dt_avg", "Average drift time;Average drift time [ns];counts", 200, 50, 100);
    TH1I * h_vec_dt = new TH1I("h_vec_dt", "Individual drift time;drift time [ns];counts", 500, 0, 250);
    TH2I * h_vec_dt_vs_avg = new TH2I("h_vec_dt_vs_avg", "Individual vs Average drift time;Individual drift time [ns];Average vector drift time [ns]", 500, 0, 250, 200, 50, 100);
    TH2I * h_straw_dt_vs_plane = new TH2I("h_straw_dt_vs_plane", "Individual drift time vs plane;Individual drift time [ns];Plane", 500, 0, 250, 16, 0, 16);

    TCanvas * c_track_qa1 = new TCanvas("c_track_qa1", "c_track_qa1", 800, 800);
    c_track_qa1->DivideSquare(6);
    TCanvas * c_track_qa2 = new TCanvas("c_track_qa2", "c_track_qa2", 1600, 800);
    c_track_qa2->DivideSquare(5);
    TCanvas * c_track_qa_paper = new TCanvas("c_track_qa_paper", "c_track_qa_paper", 800, 400);
//     c_track_qa_paper->DivideSquare(4);

    TH2I * h_straw_gea[straw_mods][straw_layers];
    TCanvas * c_straw_gmodX[straw_mods];

    TH2I * h_straw_digi[straw_mods][straw_layers];
    TCanvas * c_straw_dmodX[straw_mods];

    TH1I * h_straw_residual[straw_mods][straw_layers];
    TCanvas * c_straw_residual;
    TH1I * h_straw_residual_all;
    TH2I * h_straw_residual_all_vs_chi2;
    TCanvas * c_straw_residual_all;

    TH2I * h_t_xy, * h_t_txty, * h_rpc_len_p, * h_rpc_len_tof;
    TH1I * h_rpc_tof, * h_rpc_len, * h_p;
    TCanvas * c_t, * c_rpc;

    // no filter
    TH2I * h_rpc_mod_mult_gea = new TH2I("h_rpc_mod_mult_gea", "RPC: hit mult per module (Geant);module;multiplicity", 4, 0, 4, 35, 0, 35);
    TH2I * h_rpc_mod_mult = new TH2I("h_rpc_mod_mult", "RPC: hit mult per module;module;multiplicity", 4, 0, 4, 10, 0, 10);
    TCanvas * c_rpc_mod_mult = new TCanvas("c_rpc_mod_mult", "RPC: hit mult per module", 800, 400);
    c_rpc_mod_mult->Divide(2,1);

    TH2I * h_rpc_mod_strip_mult_gea[4];
    TH2I * h_rpc_mod_strip_mult[4];
    TCanvas * c_rpc_mod_strip_mult = new TCanvas("c_rpc_mod_strip_mult", "RPC: hit mult per module", 800, 400);
    c_rpc_mod_strip_mult->Divide(2,1);

    for (int m = 0; m < 4; ++m)
    {
        sprintf(buff1, "h_rpc_mod_strip_mult_gea_%d", m);
        sprintf(buff2, "RPC: hit mult per strip for module %d (GEA);strip;multiplicity", m);
        h_rpc_mod_strip_mult_gea[m] = new TH2I(buff1, buff2, rpc_strips, 0, rpc_strips, 20, 0, 20);

        sprintf(buff1, "h_rpc_mod_strip_mult_%d", m);
        sprintf(buff2, "RPC: hit mult per strip for module %d (reco);strip;multiplicity", m);
        h_rpc_mod_strip_mult[m] = new TH2I(buff1, buff2, rpc_strips, 0, rpc_strips, 5, 0, 5);
    }

    // filter
    TH2I * h_rpc_mod_mult_gea_f = new TH2I("h_rpc_mod_mult_gea_f", "RPC: hit mult per module (Geant);module;multiplicity", 4, 0, 4, 35, 0, 35);
    TH2I * h_rpc_mod_mult_f = new TH2I("h_rpc_mod_mult_f", "RPC: hit mult per module;module;multiplicity", 4, 0, 4, 10, 0, 10);
    TCanvas * c_rpc_mod_mult_f = new TCanvas("c_rpc_mod_mult_f", "RPC: hit mult per module", 800, 400);
    c_rpc_mod_mult_f->Divide(2,1);

    TH2I * h_rpc_mod_strip_mult_gea_f[4];
//     TH2I * h_rpc_mod_strip_mult_f[4];
    TCanvas * c_rpc_mod_strip_mult_f = new TCanvas("c_rpc_mod_strip_mult_f", "RPC: hit mult per module", 800, 400);
    c_rpc_mod_strip_mult_f->Divide(2,1);

    for (int m = 0; m < 4; ++m)
    {
        sprintf(buff1, "h_rpc_mod_strip_mult_gea_%d_f", m);
        sprintf(buff2, "RPC: hit mult per strip for module %d (GEA);strip;multiplicity", m);
        h_rpc_mod_strip_mult_gea_f[m] = new TH2I(buff1, buff2, rpc_strips, 0, rpc_strips, 6, 0, 6);

//         sprintf(buff1, "h_rpc_mod_strip_mult_%d_f", m);
//         sprintf(buff2, "RPC: hit mult per strip for module %d (reco);strip;multiplicity", m);
//         h_rpc_mod_strip_mult_f[m] = new TH2I(buff1, buff2, rpc_strips, 0, rpc_strips, 5, 0, 5);
    }

    TH2I * h_rpc_qa_uv_tof = new TH2I("h_rpc_qa_uv_tof", "Tof: gea vs reco;Tof Geant [ns];Tof Reco [ns]", 200, 0, 50, 200, 0, 50);
    TH2I * h_rpc_qa_uv_u = new TH2I("h_rpc_qa_uv_u", "u: gea vs reco;y Geant [mm];U [mm]", 100, -1000., 1000., 100, -1000., 1000.);
    TH2I * h_rpc_qa_uv_v = new TH2I("h_rpc_qa_uv_v", "v: gea vs reco;x Geant [mm];V [mm]", 65, -650., 650., 65, -650., 650.);
    TH2I * h_rpc_qa_uv_s = new TH2I("h_rpc_qa_uv_s", "strip: gea vs reco;y Geant [mm];s [strip]", 100, -1000., 1000., rpc_strips, 0, rpc_strips);
    TCanvas * c_rpc_qa_uv = new TCanvas("c_rpc_qa_uv", "c_rpc_qa_uv", 800, 800);
    c_rpc_qa_uv->DivideSquare(4);

    TH2I * h_rpc_qa_xy_tof = new TH2I("h_rpc_qa_xy_tof", "Tof: gea vs reco;Tof Geant [ns];Tof Reco [ns]", 200, 0, 50, 200, 0, 50);
    TH2I * h_rpc_qa_xy_x = new TH2I("h_rpc_qa_xy_x", "x: gea vs reco;y Geant [mm];Y [mm]", 100, -1000., 1000., 100, -1000., 1000.);
    TH2I * h_rpc_qa_xy_y = new TH2I("h_rpc_qa_xy_y", "y: gea vs reco;x Geant [mm];X [mm]", 100, -1000., 1000., 100, -1000., 1000.);
    TH2I * h_rpc_qa_xy_xy = new TH2I("h_rpc_qa_xy_xy", "x-y;x [mm];y [mm]", 100, -1000., 1000., 100, -1000., 1000.);
    TCanvas * c_rpc_qa_xy = new TCanvas("c_rpc_qa_xy", "c_rpc_qa_xz", 800, 800);
    c_rpc_qa_xy->DivideSquare(4);

    TH2I * h_rpc_gea[rpc_mods][rpc_layers];
    TCanvas * c_rpc_gea;

    TH1I * h_rpc_digi[rpc_mods][rpc_layers];
    TCanvas * c_rpc_digi;

    Float_t hole_offset[2] = { 225.0, 375.0 };
    Float_t straw_rot[2][4] = { {0, 90, 270, 0}, {0, 90, 45, -45} };
    Float_t rpc_rot[2] = { 0, 90 };
//     Float_t rpc_off[2][2] ={ -490.0, 490.0, 490.0, -490.0 };

    for (uint i = 0; i < straw_mods; ++i)
    {
        for (uint j = 0; j < straw_layers; ++j)
        {
            sprintf(buff1, "h_straw_gea_%d_%d", i, j);
            sprintf(buff2, "STRAW: x-y correlation, mod=%d, cell=%d;x [mm];y [mm]", i, j);
            h_straw_gea[i][j] = new TH2I(buff1, buff2, 60, -6, 6, 130, -650, 650);

            h_straw_gea[i][j]->GetXaxis()->SetLabelSize(0.06);
            h_straw_gea[i][j]->GetYaxis()->SetLabelSize(0.06);

            h_straw_gea[i][j]->GetXaxis()->SetTitleSize(0.06);
            h_straw_gea[i][j]->GetYaxis()->SetTitleSize(0.06);

            h_straw_gea[i][j]->GetXaxis()->SetTitleOffset(1.05);
            h_straw_gea[i][j]->GetYaxis()->SetTitleOffset(1.75);

            sprintf(buff1, "h_straw_digi_%d_%d", i, j);
            sprintf(buff2, "STRAW: x-straw correlation, mod=%d, cell=%d;x [mm];straw", i, j);
            h_straw_digi[i][j] = new TH2I(buff1, buff2, 65, -650, 650, 65, -650, 650);

            h_straw_digi[i][j]->GetXaxis()->SetLabelSize(0.06);
            h_straw_digi[i][j]->GetYaxis()->SetLabelSize(0.06);

            h_straw_digi[i][j]->GetXaxis()->SetTitleSize(0.06);
            h_straw_digi[i][j]->GetYaxis()->SetTitleSize(0.06);

            h_straw_digi[i][j]->GetXaxis()->SetTitleOffset(1.05);
            h_straw_digi[i][j]->GetYaxis()->SetTitleOffset(1.75);

            sprintf(buff1, "h_straw_residual_%d_%d", i, j);
            sprintf(buff2, "STRAW: track residual, mod=%d, layer=%d;#Delta x [mm];counts", i, j);
            h_straw_residual[i][j] = new TH1I(buff1, buff2, 100, -1, 1);
        }

        sprintf(buff1, "c_straw_gmod%d", i);
        c_straw_gmodX[i] = new TCanvas(buff1, buff1, 800, 800);
        c_straw_gmodX[i]->DivideSquare(straw_layers);

        sprintf(buff1, "c_straw_dmod%d", i);
        c_straw_dmodX[i] = new TCanvas(buff1, buff1, 800, 800);
        c_straw_dmodX[i]->DivideSquare(straw_layers);
    }

    h_straw_residual_all = new TH1I("h_straw_residual_all", "STRAW: track residual;residuals [mm];counts / 0.02 mm", 100, -1, 1);
    h_straw_residual_all_vs_chi2 = new TH2I("h_straw_residual_all_vs_chi2", "STRAW: track residual;residuals [mm];#chi^{2}/ndf", 100, -1, 1, 500, 0, 10);
    c_straw_residual = new TCanvas("c_straw_residual", "c_straw_residual", 1600, 800);
    c_straw_residual->Divide(4, 2);
    c_straw_residual_all = new TCanvas("c_straw_residual_all", "c_straw_residual_all", 800, 400);
    c_straw_residual_all->Divide(2, 1);

    h_t_xy = new TH2I("h_t_xy", "Tracking: X-Y", 65, -650., 650., 65, -650., 650.);
    h_t_txty = new TH2I("h_t_txty", "Tracking: Tx-Ty", 80, -0.2, 0.2, 80, -0.2, 0.2);

    c_t = new TCanvas("c_t", "c_t", 800, 800);
    c_t->DivideSquare(4);

    h_rpc_tof = new TH1I("h_rpc_tof", "TOF from RPC", 200, 10, 110);
    h_rpc_len = new TH1I("h_rpc_len", "Distance from RPC", 100, 7400, 7900);
    h_rpc_len_p = new TH2I("h_rpc_len_p", "RPC: Dist vs P", 100, 7400, 7900, 500, 0, 2000);
    h_rpc_len_tof = new TH2I("h_rpc_len_tof", "RPC: Dist vs TOF", 100, 7400, 7900, 80, 20, 100);
    h_p = new TH1I("h_p_gea", "Momentum reco from RPC", 250, 0, 2500);
    TH1I* h_p_gea = new TH1I("h_p", "Momentum from Geant", 250, 0, 2500);
    TH1I* h_p_rec_14 = new TH1I("h_p_rec_14", "Momentum from Geant", 250, 0, 2500);

    TH1I * h_p_res = new TH1I("h_p_res", "Momentum resolution", 100, 0, 2500);
    TH2I * h_p_res2 = new TH2I("h_p_res2", "Momentum resolution 2;p [MeV/c];#Deltap/p", 50, 0, 2500, 600, -0.2, 0.2);

    c_rpc = new TCanvas("c_rpc", "c_rpc", 800, 800);
    c_rpc->DivideSquare(4);

    TH2I * h_rpchit_xy = new TH2I("h_rpchit_xy", "RPC Hit: X-Y", 100, -1000, 1000, 100, -1000, 1000);
    TCanvas * c_rpchit = new TCanvas("c_rpchit", "c_rpchit", 800, 800);

    for (uint i = 0; i < rpc_mods; ++i)
    {
        for (uint j = 0; j < rpc_layers; ++j)
        {
            sprintf(buff1, "h_rpc_gea_%d_%d", i, j);
            sprintf(buff2, "RPC: x-y correlation, mod=%d, layer=%d;x [mm];y [mm]", i, j);
            h_rpc_gea[i][j] = new TH2I(buff1, buff2, 140, -700, 700, 140, -700, 700);

            h_rpc_gea[i][j]->GetXaxis()->SetLabelSize(0.06);
            h_rpc_gea[i][j]->GetYaxis()->SetLabelSize(0.06);

            h_rpc_gea[i][j]->GetXaxis()->SetTitleSize(0.06);
            h_rpc_gea[i][j]->GetYaxis()->SetTitleSize(0.06);

            h_rpc_gea[i][j]->GetXaxis()->SetTitleOffset(1.05);
            h_rpc_gea[i][j]->GetYaxis()->SetTitleOffset(1.75);

            sprintf(buff1, "h_rpc_digi_%d_%d", i, j);
            sprintf(buff2, "RPC: x-y correlation, mod=%d, layer=%d;strip", i, j);
            h_rpc_digi[i][j] = new TH1I(buff1, buff2, rpc_strips, 0, rpc_strips);

            h_rpc_digi[i][j]->GetXaxis()->SetLabelSize(0.06);
            h_rpc_digi[i][j]->GetYaxis()->SetLabelSize(0.06);

            h_rpc_digi[i][j]->GetXaxis()->SetTitleSize(0.06);
            h_rpc_digi[i][j]->GetYaxis()->SetTitleSize(0.06);

            h_rpc_digi[i][j]->GetXaxis()->SetTitleOffset(1.05);
            h_rpc_digi[i][j]->GetYaxis()->SetTitleOffset(1.75);
        }
    }

    sprintf(buff1, "c_rpc_gea");
    c_rpc_gea = new TCanvas(buff1, buff1, 800, 800);
    c_rpc_gea->DivideSquare(rpc_mods*rpc_layers);

    sprintf(buff1, "c_rpc_digi");
    c_rpc_digi = new TCanvas(buff1, buff1, 800, 800);
    c_rpc_digi->DivideSquare(rpc_mods*rpc_layers);

    struct PlaneStats {
        PlaneStats()
        {
            for (Int_t i = 0; i < 8; ++i)
                pl[i] = 0;
        }
        Int_t pl[8];
        Int_t count()
        {
            Int_t cnt = 0;
            for (Int_t i = 0; i < 8; ++i)
                cnt += (pl[i] > 0 ? 1 : 0);
            return cnt;
        }
    };

    for (Int_t i = limit_sta; i < limit_sto; i++)                    // event loop
    {
        if (anapars.verbose)
        {
            printf("==============================================\n" \
                   "+++ Event: %d\n" \
                   "==============================================\n", i);
        }

        /*Int_t nbytes =*/  loop->nextEvent(i);         // get next event. categories will be cleared before
        //cout << fCatGeantFwDet->getEntries() << endl;

        int tracks_num = fCatGeantKine->getEntries();
//         h_tr_mult->Fill(tracks_num);
        int active_tracks = 0;
        for (int j = 0; j < tracks_num; ++j)
        {
            HGeantKine * kine = HCategoryManager::getObject(kine, fCatGeantKine, j);
            if (kine && kine->isActive())
                ++active_tracks;
        }
        h_tr_mult->Fill(active_tracks);

        std::vector<float> straw_xcord[straw_mods][straw_layers];

        int geant_fwdet_cnt = fCatGeantFwDet->getEntries();

        HGeantFwDet * gfwdet = 0;

        PlaneStats ps[100000];
        std::map<Int_t,Int_t> used_tracks_sts, used_tracks_sts_f;
        std::map<Int_t,Int_t> used_tracks_rpc, used_tracks_rpc_f;

        Int_t mult[4] = { 0 };
        std::vector<Int_t> mult_strip[4][rpc_strips];
//         Int_t mult_strip_tracks[4][rpc_strips][100] = { 0 };
        Int_t mult_f[4] = { 0 };
        std::vector<Int_t> mult_strip_f[4][rpc_strips];
// printf("*** New event ***\n");
        // kine
        for (int j = 0; j < geant_fwdet_cnt; ++j)
        {
            gfwdet = HCategoryManager::getObject(gfwdet, fCatGeantFwDet, j);

            Char_t mod, layer, subcell;
            Int_t cell;
            Float_t hx, hy, hz, px, py, pz, tof, len, E;

            gfwdet->getAddress(mod, layer, cell, subcell);

            if (mod == 0 or mod == 1) // for straws
            {
                gfwdet->getHit(hx, hy, hz, px, py, pz, tof, len, E);

                cell = cell >> 1;

                h_straw_gea[(int)mod][(int)layer]->Fill(hx, hz);

                Int_t trk = gfwdet->getTrackNumber();
                ++used_tracks_sts[trk];

                Int_t vp = mod * 4 + layer;
                ++ps[trk].pl[vp];
            }
            if (mod == 6 or mod == 7) // for rpc
            {
                gfwdet->getHit(hx, hy, hz, px, py, pz, tof, len, E);

                Int_t trk = gfwdet->getTrackNumber();
                ++used_tracks_rpc[trk];



                HGeantKine * kine = HCategoryManager::getObject(kine, fCatGeantKine, trk-1);

                Float_t offset_x = 0.0;
                Float_t offset_y = 0.0;

                Int_t _mod = (Int_t)mod - 6;
                Float_t cosa = cos(rpc_rot[layer>>1] * TMath::DegToRad());
                Float_t sina = sin(rpc_rot[layer>>1] * TMath::DegToRad());

                Float_t xx = hx*cosa + hy*sina;
                Float_t yy = -hx*sina + hy*cosa;
                h_rpc_gea[_mod][(int)layer]->Fill(xx, yy);
                ++mult[_mod*2 + layer];
                Int_t _s = rpc_strips * ((hy + 450.)/900.);
    //            printf("hx = %f  s = %d\n", hy, _s);
                mult_strip[_mod*2 + layer][_s].push_back(trk);      // store the tracks
//                 ++mult_strip[_mod*2 + layer][_s];

//                 kine->print();
//                 printf("trk=%d  pos=%f,%f,%f p=%f,%f,%f, tof=%f len=%f, E=%f   mod=%d  lay=%d  cell=%d  scell=%d  strip=%d\n\n", trk, hx, hy, hz, px, py, pz, tof, len, E, mod, layer, cell, subcell, _s);
// printf("check 0\n");
                HGeomVector vert;
                kine->getVertex(vert);
                if ((layer < 2 && vert.Z() < 7450) or (layer >= 2 && vert.Z() < 7650))
                {
                    ++mult_f[_mod*2 + layer];
                    mult_strip_f[_mod*2 + layer][_s].push_back(trk);
                    continue;
                }
                else
                {
                    mult_strip[_mod*2 + layer][_s].push_back(kine->getParentTrack());
                }
// printf("check 1\n");
                if (mult_strip_f[_mod*2 + layer][_s].size() == 0)
                {
                    ++mult_f[_mod*2 + layer];
                    mult_strip_f[_mod*2 + layer][_s].push_back(trk);
                    mult_strip[_mod*2 + layer][_s].push_back(kine->getParentTrack());
                    continue;
                }
// printf("check 2\n");
//                 Int_t mech = kine->getMechanism();
// printf("Registered: ");for (int z = 0; z < mult_strip[_mod*2 + layer][_s].size(); ++z) printf("%d ", mult_strip[_mod*2 + layer][_s][z]); printf("\n");
                std::vector<Int_t>::const_iterator it = std::find(mult_strip[_mod*2 + layer][_s].begin(), mult_strip[_mod*2 + layer][_s].end(), kine->getParentTrack());
                if (it == mult_strip[_mod*2 + layer][_s].end())
                {
                    ++mult_f[_mod*2 + layer];
                    mult_strip_f[_mod*2 + layer][_s].push_back(trk);
                    continue;
                }
//                 printf("check 3\n");
            }
        }

        for (int j = 0; j < 4; ++j)
        {
            if (mult[j]) h_rpc_mod_mult_gea->Fill(j, mult[j]);
            if (mult_f[j]) h_rpc_mod_mult_gea_f->Fill(j, mult_f[j]);
            for (int k = 0; k < rpc_strips; ++k)
            {
                clean(mult_strip[j][k]);
                clean(mult_strip_f[j][k]);

                if (mult_strip[j][k].size()) h_rpc_mod_strip_mult_gea[j]->Fill(k, mult_strip[j][k].size());
//                 if (mult_strip_f[j][k].size()) printf("Fill: %d  %d\n", k, mult_strip_f[j][k].size());
                if (mult_strip_f[j][k].size()) h_rpc_mod_strip_mult_gea_f[j]->Fill(k, mult_strip_f[j][k].size());
                if (mult_strip_f[j][k].size() > 5)
                {
                    printf("\n\n\n[%d] Mult = %lu\n\n", i, mult_strip_f[j][k].size());
                    for (int z = 0; z < mult_strip_f[j][k].size(); ++z)
                    {
                        HGeantKine * kine = HCategoryManager::getObject(kine, fCatGeantKine, mult_strip_f[j][k][z]-1);
                        kine->print();
                    }
                }
            }
        }

        int good_tracks = -1;   // -1 measn that there was no cal category

        if (fFwDetStrawCal)
        {
            good_tracks = 0;
            // digitizer
            int fwdet_calsim_cnt = fFwDetStrawCal->getEntries();

            HFwDetStrawCalSim * fwdetstrawcs = 0;

            for (int j = 0; j < fwdet_calsim_cnt; ++j)
            {
                fwdetstrawcs = HCategoryManager::getObject(fwdetstrawcs, fFwDetStrawCal, j);

                Char_t mod, l, p, ud;
                Int_t straw;
                Float_t time, adc, U, Z;
                Float_t lab_x, lab_y, lab_z;

                fwdetstrawcs->getAddress(mod, l, p, straw, ud);
                fwdetstrawcs->getHit(time, adc, U, Z);

                ((HFwDetStrawCalSim *)fwdetstrawcs)->getHitPos(lab_x, lab_y, lab_z);
                Int_t vp = fwdetstrawcs->getVPlane();
                h_straw_dt_vs_plane->Fill(fwdetstrawcs->getTime(), vp);

                Float_t offset_x = 0.0;
                Float_t offset_y = 0.0;

                switch (ud)
                {
                    case 1:
                        offset_x = hole_offset[(int)mod] * -sin(straw_rot[(int)mod][(int)l] * 3.1415/180.0);
                        offset_y = hole_offset[(int)mod] * cos(straw_rot[(int)mod][(int)l] * 3.1415/180.0);
                        break;
                    case 2:
                        offset_x = -hole_offset[(int)mod] * -sin(straw_rot[(int)mod][(int)l] * 3.1415/180.0);
                        offset_y = -hole_offset[(int)mod] * cos(straw_rot[(int)mod][(int)l] * 3.1415/180.0);
                        break;
                    default:
                        break;
                }

                h_straw_digi[(int)mod][(int)l]->Fill(lab_x, lab_y);
            }

            std::map<Int_t,Int_t>::iterator tr_it;
            for (tr_it = used_tracks_sts.begin(); tr_it != used_tracks_sts.end(); ++tr_it)
            {
                Int_t trk_num = tr_it->first;
                h_tr_sts_planes->Fill(ps[trk_num].count());
                if (ps[trk_num].count() == 8) ++good_tracks;
            }
        }

        h_tr_sts_mult->Fill(used_tracks_sts.size());
        h_tr_sts_mult_vs_hits->Fill(used_tracks_sts.size(), geant_fwdet_cnt);
        h_tr_sts_num->SetBinContent(1, h_tr_sts_num->GetBinContent(1) + used_tracks_sts.size());

        h_tr_rpc_mult->Fill(used_tracks_rpc.size());

        h_tr_sts_vs_rpc_mult->Fill(used_tracks_sts.size(), used_tracks_rpc.size());

        if (fFwDetRpcHit)
        {
            Int_t hcnt = fFwDetRpcHit->getEntries();

            HFwDetRpcHit * fwdetrpchit = 0;

            for (int j = 0; j < hcnt; ++j)
            {
                fwdetrpchit = HCategoryManager::getObject(fwdetrpchit, fFwDetRpcHit, j);

                h_rpchit_xy->Fill(fwdetrpchit->getX(), fwdetrpchit->getY());
            }
        }

        Int_t vcnt = 0;
        Int_t vcnt_good = 0;
        if (fFwDetCand)
        {
            vcnt = fFwDetCand->getEntries();

            HFwDetCand * fwdetvec = 0;
            HFwDetCandSim * fwdetvecsim = 0;

            for (int j = 0; j < vcnt; ++j)
            {
                fwdetvec = HCategoryManager::getObject(fwdetvec, fFwDetCand, j);

                Float_t dt_avg = 0.0;
                Int_t nhits = fwdetvec->getNofHits();
                for (Int_t k = 0; k < nhits; ++k)
                {
                    Int_t idx = fwdetvec->getHitIndex(k);
                    HFwDetStrawCal * straw_cal = HCategoryManager::getObject(straw_cal, fFwDetStrawCal, idx);
                    dt_avg += straw_cal->getTime();
                }
                dt_avg /= nhits;
                for (Int_t k = 0; k < nhits; ++k)
                {
                    Int_t idx = fwdetvec->getHitIndex(k);
                    HFwDetStrawCal * straw_cal = HCategoryManager::getObject(straw_cal, fFwDetStrawCal, idx);
                    Float_t dt = straw_cal->getTime();
                    h_vec_dt->Fill(dt);
                    h_vec_dt_vs_avg->Fill(dt, dt_avg);
                }

                h_vec_dt_avg->Fill(dt_avg);

                h_t_xy->Fill(fwdetvec->getBaseX(), fwdetvec->getBaseY());
                h_t_txty->Fill(fwdetvec->getDirTx(), fwdetvec->getDirTy());

                // residuals

                if (fwdetvec->getTofRec() == 1)
                {
                    Float_t bx = fwdetvec->getBaseX();
                    Float_t by = fwdetvec->getBaseY();
                    Float_t bz = fwdetvec->getBaseZ();
                    Float_t dx = fwdetvec->getDirTx();
                    Float_t dy = fwdetvec->getDirTy();
// fwdetvec->print();
                    bool print_vec = false;
                    for (Int_t k = 0; k < nhits; ++k)
                    {
                        Char_t m, l, p, ud; Int_t s;
                        Int_t idx = fwdetvec->getHitIndex(k);
                        HFwDetStrawCal * straw_cal = HCategoryManager::getObject(straw_cal, fFwDetStrawCal, idx);
                        straw_cal->getAddress(m, l, p, s, ud);

                        Float_t cell_x = pStrawGeomPar->getOffsetX(m, l, p) + s * pStrawGeomPar->getStrawPitch(m);
                        Float_t cell_z = pStrawGeomPar->getOffsetZ(m, l, p);
                        TVector2 p_p(cell_z, cell_x);       // wire position      p
//     PR(cell_z);PR(cell_x);PR(s);
                        Float_t u_b = bx * cosa[(int)m][(int)l] + by * sina[(int)m][(int)l];
                        Float_t v_b = - bx * sina[(int)m][l] + by * cosa[(int)m][(int)l];
                        Float_t u_d = dx * cosa[(int)m][(int)l] + dy * sina[(int)m][(int)l];//PR(u_d);
                        Float_t v_d = - dx * sina[(int)m][(int)l] + dy * cosa[(int)m][(int)l];//PR(v_d);

                        TVector2 v_n(1.0, u_d);   // track vector dir.  n
    //                     v_n.Print();
                        v_n = v_n/v_n.Mod();
                        TVector2 p_a(bz, u_b);         // track hit position a

                        // calculations:
                        // dist = | (a-p) - ((a-p)*n)n|
                        TVector2 a_p_diff = p_a - p_p;
                        TVector2 v_proj_n_diff = (a_p_diff * v_n) * v_n;
                        TVector2 v_dist = a_p_diff - v_proj_n_diff;
                        Float_t radius = v_dist.Mod();
                        Float_t dr = fwdetvec->getStrawDriftRadius(k);
    //                     p_a.Print();p_p.Print();
    //                     PR(radius);
                        h_straw_residual[(int)m][(int)l]->Fill(radius - dr);
                        h_straw_residual_all->Fill(radius - dr);
                        h_straw_residual_all_vs_chi2->Fill(radius - dr, fwdetvec->getChi2()/fwdetvec->getNDF() * (0.2*0.2/0.12/0.12));
    //                     Float_t dt = straw_cal->getTime();
    //                     h_vec_dt->Fill(dt);
    //                     h_vec_dt_vs_avg->Fill(dt, dt_avg);

//                         printf("straw: %d %d %d %d %d   cell: z=%f  x,y=%f   track: z=%f  x=%f  y=%f   uv_d=%f,%f   uv_b=%f,%f   bz=%f\n", (int)m, (int)l, (int)p, (int)s, ud, cell_z, cell_x, cell_z, bx + (cell_z - bz)*dx, by + (cell_z - bz)*dy, u_d, v_d, u_b, v_b, bz);
                        if (abs(radius - dr) > 5.0)
                        {
//                             PR((radius - dr));
//                             print_vec = true;
                        }
                    }
                    if (print_vec)
                    {
                        HFwDetCandSim * vsim = dynamic_cast<HFwDetCandSim*>(fwdetvec);
                        if (vsim) vsim->print();
                        else fwdetvec->print();
                    }
                }

//                 TVector2 v_n(gf.pzHit, gf.pxHit);   // track vector dir.  n
//                 v_n = v_n/v_n.Mod();
//                 TVector2 p_a(hit_z, hit_x);         // track hit position a
//                 TVector2 p_p(cell_z, cell_x);       // wire position      p

                // residuals end
                if (fwdetvec->getTofRec())
                {
                    HFwDetCandSim * vsim = dynamic_cast<HFwDetCandSim*>(fwdetvec);
                    if (vsim)
                        fwdetvec->calc4vectorProperties(HPhysicsConstants::mass(vsim->getGeantPID()));
                    else
                        fwdetvec->calc4vectorProperties(140);

                    h_rpc_tof->Fill(fwdetvec->getTof());
                    h_rpc_len->Fill(fwdetvec->getDistance());
                    h_rpc_len_p->Fill(fwdetvec->getDistance(), fwdetvec->P());
                    h_rpc_len_tof->Fill(fwdetvec->getDistance(), fwdetvec->getTof());
                    h_p->Fill(fwdetvec->P());

                    if (vsim and fwdetvec->getTofRec() > 0 and vsim->getGeantPID() == 14) {
                        Float_t px = vsim->getGeantPx1();
                        Float_t py = vsim->getGeantPy1();
                        Float_t pz = vsim->getGeantPz1();
                        Float_t p = sqrt(px*px + py*py + pz*pz);
                        Float_t p_reco = fwdetvec->P();
                        h_p_res2->Fill(p, (p_reco-p)/p);
                        h_p_gea->Fill(p);
                        h_p_rec_14->Fill(p_reco);
                    }
                }
                else
                {
                    h_vec_chi2_notofrec->Fill(fwdetvec->getChi2()/fwdetvec->getNDF());
                }

                h_vec_chi2->Fill(fwdetvec->getChi2()/fwdetvec->getNDF());
                h_vec_chi2_fixed->Fill(fwdetvec->getChi2()/fwdetvec->getNDF() * (0.2*0.2/0.12/0.12));

                fwdetvecsim = dynamic_cast<HFwDetCandSim*>(fwdetvec);
                if (fwdetvecsim)
                {
                    Int_t gea_cors = fwdetvecsim->calcGeantCorrTrackIds();
                    h_vec_gea_cors->Fill(gea_cors);
                    if (gea_cors == 1)
                    {
                        h_vec_chi2_good->Fill(fwdetvec->getChi2()/fwdetvec->getNDF());

                        ++vcnt_good;
                    }
                }
                if (fwdetvecsim and anapars.verbose)
                    fwdetvecsim->print();
            }

            h_vec_sts_mult->Fill(vcnt);
            h_vec_sts_num->SetBinContent(1, h_vec_sts_num->GetBinContent(1) + vcnt);
            h_vec_sts_good_num->SetBinContent(1, h_vec_sts_good_num->GetBinContent(1) + vcnt_good);
        }

        h_tr_vec_sts_mult->Fill(used_tracks_sts.size(), vcnt);
        h_tr_good_vec_sts_mult->Fill(good_tracks, vcnt);
        h_tr_good_vec_sts_good_mult->Fill(good_tracks, vcnt_good);

        if (anapars.show_fakes and good_tracks != vcnt)
            printf("!!! Bad number of vectors: expected %d  got %d  at event %d\n", good_tracks, vcnt, i);

        if (fFwDetRpcCal)
        {
            int fwdet_rpccalsim_cnt = fFwDetRpcCal->getEntries();

            HFwDetRpcCalSim * fwdetrpccs = 0;
            Int_t mult[4] = { 0 };
            Int_t mult_strip[4][rpc_strips] = { 0 };

            for (int j = 0; j < fwdet_rpccalsim_cnt; ++j)
            {
                fwdetrpccs = HCategoryManager::getObject(fwdetrpccs, fFwDetRpcCal, j);

                Char_t mod, l, s;
                Float_t lab_x = 0, lab_y = 0, lab_t = 0;

                fwdetrpccs->getAddress(mod, l, s);
                Float_t U = fwdetrpccs->getU(0);
                Float_t V = fwdetrpccs->getV(0);

                Float_t X = fwdetrpccs->getX(0);
                Float_t Y = fwdetrpccs->getY(0);

                Float_t t = fwdetrpccs->getTof(0);

                fwdetrpccs->getHit(0, lab_x, lab_y, lab_t);

                h_rpc_digi[(int)mod][(int)l]->Fill(s);

                Float_t cosa = cos(rpc_rot[(uint)mod] * TMath::DegToRad());
                Float_t sina = sin(rpc_rot[(uint)mod] * TMath::DegToRad());

                Float_t xx = lab_x*cosa + lab_y*sina;
                Float_t yy = -lab_x*sina + lab_y*cosa;

                h_rpc_qa_uv_u->Fill(yy, U);
                h_rpc_qa_uv_v->Fill(xx, V);
                h_rpc_qa_uv_tof->Fill(lab_t, t);
                h_rpc_qa_uv_s->Fill(yy, s);

                h_rpc_qa_xy_x->Fill(lab_x, X);
                h_rpc_qa_xy_y->Fill(lab_y, Y);
                h_rpc_qa_xy_tof->Fill(lab_t, t);
                h_rpc_qa_xy_xy->Fill(X, Y);
                ++mult[mod*2+l];
                mult_strip[mod*2+l][(uint)s] = fwdetrpccs->getHitsNum();
            }
            for (int j = 0; j < 4; ++j)
            {
                if (mult[j]) h_rpc_mod_mult->Fill(j, mult[j]);
                for (uint k = 0; k < rpc_strips; ++k)
                    if (mult_strip[j][k]) h_rpc_mod_strip_mult[j]->Fill(k, mult_strip[j][k]);
            }
        }

    } // end eventloop

    // Write
    // geant straws
    for (uint i = 0; i < straw_mods; ++i)
    {
        for (uint j = 0; j < straw_layers; ++j)
        {
            c_straw_gmodX[i]->cd(1+j);
            gPad->SetLeftMargin(0.2);
            gPad->SetBottomMargin(0.2);

            h_straw_gea[i][j]->Draw("colz");
            h_straw_gea[i][j]->Write();
        }
        c_straw_gmodX[i]->Write();
    }
    // geant rpc
    for (uint i = 0; i < rpc_mods; ++i)
    {
        for (uint j = 0; j < rpc_layers; ++j)
        {
            c_rpc_gea->cd(1 + i*rpc_layers + j);
            gPad->SetLeftMargin(0.2);
            gPad->SetBottomMargin(0.2);

            h_rpc_gea[i][j]->Draw("colz");
            h_rpc_gea[i][j]->Write();
        }
    }
    c_rpc_gea->Write();

    for (uint i = 0; i < rpc_mods; ++i)
    {
        for (uint j = 0; j < rpc_layers; ++j)
        {
            c_rpc_digi->cd(1 + i*rpc_layers + j);
            gPad->SetLeftMargin(0.2);
            gPad->SetBottomMargin(0.2);

            h_rpc_digi[i][j]->Draw();
            h_rpc_digi[i][j]->Write();
        }
    }
    c_rpc_digi->Write();

    TF1 * f_gaus = new TF1("f_gaus", "gaus", -5, 5);
    TLatex * tex = new TLatex;
    tex->SetNDC(kTRUE);
    // digitizer
    for (uint i = 0; i < straw_mods; ++i)
    {
        for (uint j = 0; j < straw_layers; ++j)
        {
            // digitizer
            c_straw_dmodX[i]->cd(1+j);
            gPad->SetLeftMargin(0.2);
            gPad->SetBottomMargin(0.2);

            h_straw_digi[i][j]->Draw("colz");
            h_straw_digi[i][j]->Write();

            f_gaus->SetParameters(10000, 0.0, 0.15);
            c_straw_residual->cd(1 + i*4 + j);
            h_straw_residual[i][j]->Fit(f_gaus, "Q", "", -2, 2);
            h_straw_residual[i][j]->Draw();
            tex->DrawLatex(0.6, 0.5, TString::Format("#sigma = %f", f_gaus->GetParameter(2)));
            h_straw_residual[i][j]->Write();
        }
        c_straw_dmodX[i]->Write();
    }
    c_straw_residual->Write();
    c_straw_residual_all->cd(1);
    f_gaus->SetParameters(10000, 0.0, 0.15);
    h_straw_residual_all->Fit(f_gaus, "Q", "", -2, 2);
    h_straw_residual_all->Draw();
    tex->DrawLatex(0.6, 0.5, TString::Format("#sigma = %f", f_gaus->GetParameter(2)));
    h_straw_residual_all->Write();

    c_straw_residual_all->cd(2);
    h_straw_residual_all_vs_chi2->Draw("colz");
    h_straw_residual_all_vs_chi2->Write();

    c_straw_residual_all->Write();

    c_t->cd(1);
    h_t_xy->Draw("colz");
    c_t->cd(2);
    h_t_txty->Draw("colz");
    c_t->cd(3);
    h_p->Draw();
    h_p_gea->Draw("same");
    h_p_gea->SetLineColor(2);
    h_p_rec_14->Draw("same");
    h_p_rec_14->SetLineColor(3);

    TLegend * leg_p = new TLegend(0.5, 0.5, 0.85, 0.85);
    leg_p->AddEntry(h_p, "All tracks", "l");
    leg_p->AddEntry(h_p_rec_14, "Protons", "l");
    leg_p->AddEntry(h_p_gea, "Protons from Geant", "l");
    leg_p->Draw();

    c_t->cd(4);
    h_p_res2->Draw("colz");

    c_rpc->cd(1);
    h_rpc_tof->Draw();
    c_rpc->cd(2);
    h_rpc_len->Draw();
    c_rpc->cd(3);
    h_rpc_len_p->Draw("colz");
    c_rpc->cd(4);
    h_rpc_len_tof->Draw("colz");



    h_tr_sts_mult->SetTitle("Tracks multiplicity in the STS (blue) and RPC (red) detectors");
    c_tr_fd_mult->cd(1)->SetLogy();
    h_tr_sts_mult->Draw("text20,h");
    h_tr_rpc_mult->Draw("text20,h,same");
    h_tr_sts_mult->SetLineWidth(2);
    h_tr_rpc_mult->SetLineWidth(2);
    h_tr_rpc_mult->SetLineColor(kRed);

    c_tr_fd_mult->cd(2)->SetLogz();
    h_tr_sts_vs_rpc_mult->Draw("colz,text");

    c_tr_fd_mult->Write();

    TF1 *fchi2 = new TF1("chi-square distribution", ChiSquareDistr, 0.0, 10, 3);
    fchi2->SetParameter(0, 12.0);
    fchi2->SetParameter(1, 6500.0);
    fchi2->SetParameter(2, 12.0);
    fchi2->SetLineColor(30);

    c_track_qa1->cd(1);
    h_tr_sts_mult->Draw("h,text");
    gPad->SetLogy();
    c_track_qa1->cd(2);
    h_tr_mult->Draw("h,text");
    gPad->SetLogy();
    c_track_qa1->cd(3);
    h_tr_sts_num->Draw("h,text");
    c_track_qa1->cd(4);
    h_tr_sts_planes->Draw("h,text");
    c_track_qa1->cd(5);
    h_vec_sts_mult->Draw("h,text");
    c_track_qa1->cd(6);
    h_vec_sts_num->Draw("h,text");
    c_track_qa2->cd(1);
    h_tr_vec_sts_mult->Draw("colz,text");
    gPad->SetLogz();
    c_track_qa2->cd(2);
    h_tr_good_vec_sts_mult->Draw("colz,text");
    gPad->SetLogz();
    c_track_qa2->cd(3);
    h_tr_good_vec_sts_good_mult->Draw("colz,text");
    gPad->SetLogz();
    c_track_qa2->cd(4);
    h_vec_chi2->Draw();
    h_vec_chi2_good->Draw("same");
    h_vec_chi2_good->SetLineColor(kRed);
    fchi2->Draw("same");
    c_track_qa2->cd(5);
    h_vec_gea_cors->DrawNormalized("h,text");
    c_track_qa2->cd(6);
    h_tr_sts_planes->Draw("h,text");
    c_track_qa1->Write();
    c_track_qa2->Write();

    TGaxis::SetMaxDigits(3);

    c_track_qa_paper->cd();
    h_vec_chi2->Draw();
    h_vec_chi2_fixed->Draw("same");
    h_vec_chi2->SetLineColor(12);
    h_vec_chi2_fixed->SetLineColor(1);
    h_vec_chi2->SetLineWidth(2);
    h_vec_chi2_fixed->SetLineWidth(2);
    h_vec_chi2->SetLineStyle(7);
    format_h_1_1_2_1(h_vec_chi2);
    TLegend * leg_chi2 = new TLegend(0.5, 0.7, 0.9, 0.9);
    leg_chi2->SetBorderSize(0);
    leg_chi2->AddEntry(h_vec_chi2, "#chi^{2} for #sigma = 200 #mum", "lpf");
    leg_chi2->AddEntry(h_vec_chi2_fixed, "#chi^{2} for #sigma = 119 #mum", "lpf");
    leg_chi2->Draw();

    TGaxis::SetMaxDigits(5);

//     c_track_qa_paper->cd(2);
//     format_h(h_vec_gea_cors);
//     h_vec_gea_cors->DrawNormalized("text");
//     h_vec_gea_cors->SetMarkerSize(0.3);

    c_track_qa_paper->Write();

    printf("Tracks=%.0f  vectors=%.0f  (%.1f%%)\n", h_tr_sts_num->Integral(), h_vec_sts_num->Integral(), 100.0 * h_vec_sts_num->Integral() / h_tr_sts_num->Integral());
    printf("Valid= %.0f  vectors=%.0f  (%.1f%%)\n", h_tr_sts_planes->GetBinContent(8), h_vec_sts_num->Integral(), 100.0 * h_vec_sts_num->Integral() / h_tr_sts_planes->GetBinContent(8));
    printf("Valid= %.0f  good vs=%.0f  (%.1f%%)\n", h_tr_sts_planes->GetBinContent(8), h_vec_sts_good_num->Integral(), 100.0 * h_vec_sts_good_num->Integral() / h_tr_sts_planes->GetBinContent(8));

    h_t_xy->Write();
    h_t_txty->Write();
    c_t->Write();

    h_rpc_tof->Write();
    h_rpc_len->Write();
    h_rpc_len_p->Write();
    h_rpc_len_tof->Write();
    h_p->Write();
    h_p_gea->Write();
    h_p_res2->Write();
    h_p_rec_14->Write();
    c_rpc->Write();

    c_rpchit->cd();
    h_rpchit_xy->Draw("colz");
    h_rpchit_xy->Write();
    c_rpchit->Write();

    c_rpc_qa_uv->cd(1);
    h_rpc_qa_uv_u->Draw("colz");
    h_rpc_qa_uv_u->Write();
    c_rpc_qa_uv->cd(2);
    h_rpc_qa_uv_v->Draw("colz");
    h_rpc_qa_uv_v->Write();
    c_rpc_qa_uv->cd(3);
    h_rpc_qa_uv_tof->Draw("colz");
    h_rpc_qa_uv_tof->Write();
    c_rpc_qa_uv->cd(4);
    h_rpc_qa_uv_s->Draw("colz");
    h_rpc_qa_uv_s->Write();
    c_rpc_qa_uv->Write();

    c_rpc_qa_xy->cd(1);
    h_rpc_qa_xy_x->Draw("colz");
    h_rpc_qa_xy_x->Write();
    c_rpc_qa_xy->cd(2);
    h_rpc_qa_xy_y->Draw("colz");
    h_rpc_qa_xy_y->Write();
    c_rpc_qa_xy->cd(3);
    h_rpc_qa_xy_tof->Draw("colz");
    h_rpc_qa_xy_tof->Write();
    c_rpc_qa_xy->cd(4);
    h_rpc_qa_xy_xy->Draw("colz");
    h_rpc_qa_xy_xy->Write();
    c_rpc_qa_xy->Write();

    c_rpc_mod_mult->cd(1);
    h_rpc_mod_mult_gea->Draw("colz,text");
    c_rpc_mod_mult->cd(2);
    h_rpc_mod_mult->Draw("colz,text");
    c_rpc_mod_mult->Write();
    h_rpc_mod_mult_gea->Write();
    h_rpc_mod_mult->Write();

    TVirtualPad * p1 = c_rpc_mod_strip_mult->cd(1);
    p1->Divide(2,2);
    for (int k = 0; k < 4; ++k)
    {
        p1->cd(1+k);
        h_rpc_mod_strip_mult_gea[k]->Draw("colz,text90");
        h_rpc_mod_strip_mult_gea[k]->Write();
    }
    p1 = c_rpc_mod_strip_mult->cd(2);
    p1->Divide(2,2);
    for (int k = 0; k < 4; ++k)
    {
        p1->cd(1+k);
        h_rpc_mod_strip_mult[k]->Draw("colz,text90");
        h_rpc_mod_strip_mult[k]->Write();
    }
    c_rpc_mod_strip_mult->Write();


    c_rpc_mod_mult_f->cd(1);
    h_rpc_mod_mult_gea_f->Draw("colz,text");
    c_rpc_mod_mult_f->cd(2);
    h_rpc_mod_mult->Draw("colz,text");
//     c_rpc_mod_mult->Write();
    h_rpc_mod_mult_gea_f->Write();
    h_rpc_mod_mult_f->Write();

    p1 = c_rpc_mod_strip_mult_f->cd(1);
    p1->Divide(2,2);
    for (int k = 0; k < 4; ++k)
    {
        p1->cd(1+k);
        h_rpc_mod_strip_mult_gea_f[k]->Draw("colz,text90");
        h_rpc_mod_strip_mult_gea_f[k]->Write();
    }
    p1 = c_rpc_mod_strip_mult_f->cd(2);
    p1->Divide(2,2);
    for (int k = 0; k < 4; ++k)
    {
        p1->cd(1+k);
        h_rpc_mod_strip_mult[k]->Draw("colz,text90");
//         h_rpc_mod_strip_mult[k]->Write();
    }
    c_rpc_mod_strip_mult_f->Write();

    h_tr_mult->Write();

    h_tr_sts_mult->Write();
    h_tr_sts_mult_vs_hits->Write();
    h_tr_sts_num->Write();
    h_tr_sts_planes->Write();
    h_vec_sts_mult->Write();
    h_vec_sts_num->Write();
    h_vec_sts_good_num->Write();
    h_tr_vec_sts_mult->Write();
    h_tr_good_vec_sts_mult->Write();
    h_tr_good_vec_sts_good_mult->Write();
    h_vec_gea_cors->Write();
    h_vec_chi2->Write();
    h_vec_chi2_fixed->Write();
    h_vec_chi2_good->Write();
    h_vec_chi2_notofrec->Write();
    h_vec_dt_avg->Write();
    h_vec_dt->Write();
    h_vec_dt_vs_avg->Write();
    h_straw_dt_vs_plane->Write();
    h_tr_rpc_mult->Write();
    h_tr_sts_vs_rpc_mult->Write();

    output_file->Close();
    cout << "writing root tree done" << endl;

    timer.Stop();
    timer.Print();

    return 0;
}

#!/bin/bash

ofile="../fwdetparams_full.txt"

cat fwdetparams_common.txt > $ofile
cat fwdetparams_digi.txt >> $ofile
cat fwdetparams_dst.txt >> $ofile

cat fwdet_straw_calrunpar.txt >> $ofile

cat fwdet_lookup_sts.txt >> $ofile
cat fwdet_lookup_rpc.txt >> $ofile

cat fwdet_straw_trb3calpar.txt >> $ofile
#cat fwdet_rpc_trb3calpar.txt >> $ofile

#!/bin/bash

ofile=fwdet_lookup_rpc.txt

cat <<EOF > $ofile
##############################################################################
# Lookup table for the TRB3 unpacker of the FwDet RPC detector
# Format:
# trbnet-address  channel  module  layer  strip  lrside
##############################################################################
[FwDetRpcTrb3Lookup]
// Parameter Context: FwDetRpcTrb3LookupProduction
//----------------------------------------------------------------------------
EOF

cat lookup/rpc_lay{0,1,2,3}.txt >> $ofile

cat <<EOF >> $ofile
##############################################################################
EOF

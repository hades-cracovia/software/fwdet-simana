#!/bin/bash

ofile=fwdetparams_geom.txt

truncate -s0 $ofile

cat <<EOF >> $ofile
#########################################################
[FwDetStrawGeomPar]
// ------------------------------------------------------
EOF

cat geom/fwdet_sts.geo >> $ofile

cat <<EOF >> $ofile
#########################################################
EOF

cat <<EOF >> $ofile
#########################################################
[FwDetRpcGeomPar]
// ------------------------------------------------------
EOF

cat geom/fwdet_rpc.geo >> $ofile

cat <<EOF >> $ofile
#########################################################
EOF



// base
#include "hades.h"
#include "hruntimedb.h"
#include "htask.h"
#include "hevent.h"
#include "hcategory.h"
#include "hdatasource.h"
#include "hdst.h"
#include "htime.h"
#include "hsrckeeper.h"
#include "hspectrometer.h"
#include "hfwdetdetector.h"

// tasksets
#include "hstarttaskset.h"
#include "hrichtaskset.h"
#include "hmdctaskset.h"
#include "htoftaskset.h"
#include "hrpctaskset.h"
#include "hshowertaskset.h"
#include "hwalltaskset.h"
#include "hsplinetaskset.h"
#include "hfwdettaskset.h"

//tasks
#include "hmdcdedx2maker.h"
#include "hmdctrackdset.h"
#include "hmdc12fit.h"
#include "hmdccalibrater1.h"
#include "hmetamatchF2.h"
#include "hparticlevertexfind.h"
#include "hparticlecandfiller.h"
#include "hparticletrackcleaner.h"
#include "hparticleevtinfofiller.h"
#include "hparticlebt.h"
#include "hparticlestart2hitf.h"
#include "hstart2calibrater.h"
#include "hqamaker.h"
#include "hstart2hitfsim.h"


// defs
#include "haddef.h"
#include "richdef.h"
#include "hmdcdef.h"
#include "hmdctrackddef.h"
#include "hmdctrackgdef.h"
#include "showerdef.h"
#include "rpcdef.h"
#include "tofdef.h"
#include "walldef.h"
#include "fwdetdef.h"


// containers
#include "hmdcsetup.h"
#include "hmdclayercorrpar.h"
#include "htrbnetaddressmapping.h"
#include "hpiontrackerbeampar.h"
#include "hstart2calrunpar.h"
#include "hpiontrackercalrunpar.h"

// ROOT
#include "TSystem.h"
#include "TROOT.h"
#include "TString.h"
#include "TStopwatch.h"
#include "TDatime.h"

// standard
#include <iostream>
#include <cstdio>

using namespace std;


Int_t analysisDST(TString inFile, TString outdir,Int_t nEvents=100000, Int_t startEvt=0)
{
    new Hades;
    TStopwatch timer;
    gHades->setTreeBufferSize(8000);
    gHades->makeCounter(100);
    HRuntimeDb *rtdb = gHades -> getRuntimeDb();

    gHades->setBeamTimeID(Particle::kJul14);
    gHades->getSrcKeeper()->addSourceFile("analysisDST.cc");
    gHades->getSrcKeeper()->addSourceFile("sendScript.sh");

    cout<<"InFile      "<<inFile.Data()<<endl;
    cout<<"OutDir      "<<outdir.Data()<<endl;
    cout<<"nEvents     "<<nEvents<<endl;
    cout<<"start Event "<<startEvt<<endl;

    //####################################################################
    //######################## CONFIGURATION #############################
    printf("Setting configuration...+++\n");

    TString asciiParFile     = "fwdetparams.txt";
    TString rootParFile      = "fulldst/allParam_JUL14_gen2_22022016_145750_refID_14000.root";  // gen2a
    TString paramSource      = "root,ascii"; // root, ascii, oracle

    TString outFileSuffix    = "_dst_fwdet.root";

    TString beamtime         = "jul14";
    TString paramrelease     = "JUL14SIM_gen2a";  // now, APR12_gen2_dst

    Int_t  refId             = 14000; // pimc=14000, pimcu=14001,pimw=14002,pimpe=14003
    Bool_t kParamFile        = kFALSE;
    Bool_t doExtendedFit     = kTRUE; // switch on/off fit for initial params of segment fitter (10 x slower!)
    Bool_t doMetaMatch       = kFALSE;  // default : kTRUE, kFALSE switch off metamatch in clusterfinder
    Bool_t doMetaMatchScale  = kTRUE;
    Float_t metaMatchScale   = 2;
    Float_t StartResolution  = 0.3; // 300 ps
    //####################################################################
    //####################################################################




    //-------------- Default Settings for the File names -----------------
    TString baseDir = outdir;
    if(!baseDir.EndsWith("/")) baseDir+="/";
    TString outDir   = baseDir;
    TString outDirQA = outDir+"qa/";
    //--------------------------------------------------------------------

    //------ extended output for eventbuilder 1------------------------------------//
    //----------------------------------------------------------------------------//
    Cat_t notPersistentCat[] =
    {
        //catRich, catMdc, catShower, catTof, catTracks,
        catRichRaw, catRichHitHdr,
        //catRichDirClus,catRichHit, catRichCal,
        catMdcRaw,
        catMdcCal1,
        catMdcCal2,catMdcClusInf,catMdcHit,
        //catMdcSeg, catMdcTrkCand,
        catMdcRawEventHeader,
        //catShowerCal, catShowerPID, catShowerHitHdr, catShowerRaw,
        //catShowerHit,
        //catTofRaw,
        //catTofHit, catTofCluster,
        //catRpcRaw,
        //catRpcCal,catRpcHit, catRpcCluster,
        catRKTrackB,catSplineTrack,
        //catMetaMatch,
        //catParticleCandidate, catParticleEvtInfo, catParticleMdc,
        //catWallRaw, catWallOneHit, catWallCal,
        //catStart2Raw //catStart2Cal, catStart2Hit,
        //catTBoxChan,
        //catPionTrackerRaw,catPionTrackerCal,
        //catPionTrackerHit,catPionTrackerTrack,
        //catFwDetStrawCal,catFwDetStrawVector,
        //catFwDetRpcCal,catFwDetScinCal
    };
    //--------------------------------------------------------------------

    //------------- Operations on the filenames --------------------------
    TString rootSuffix =".root";
    TString nFile;    // needed to build outputfile
    TString dirname;  // needed for datasource
    TString filename; // needed for datasource
    TString outFile;

    Int_t sourcetype = 3; // root source

    if(inFile.Contains(",")) { // comma seperated list for geant merge source
        sourcetype = 4;
        inFile.ReplaceAll(" ","");
        TObjArray* ar = inFile.Tokenize(",");
        TString firstfile;
        if(ar) {
            if(ar->GetEntries()>0) {
                firstfile = ((TObjString*)ar->At(0))->GetString();
            }
            delete ar;
        }
        nFile     = gSystem->BaseName(firstfile.Data());
        filename  = inFile;
        dirname   = "";

    } else {  // root source

        nFile     = gSystem->BaseName(inFile.Data());
        filename  = gSystem->BaseName(inFile.Data());
        dirname   = gSystem->DirName(inFile.Data());
    }
    if (nFile.EndsWith(rootSuffix)) nFile.ReplaceAll(rootSuffix,"");
    outFile  = outDir+nFile+outFileSuffix;
    outFile.ReplaceAll("//", "/");

    if(gSystem->AccessPathName(outDir.Data()) != 0) {
        cout<<"Creating output dir :"<<outDir.Data()<<endl;
        gSystem->Exec(Form("mkdir -p %s",outDir.Data()));
    }
    if(gSystem->AccessPathName(outDirQA.Data()) != 0) {
        cout<<"Creating output qadir :"<<outDirQA.Data()<<endl;
        gSystem->Exec(Form("mkdir -p %s",outDirQA.Data()));
    }
    //--------------------------------------------------------------------

    //####################################################################
    //####################################################################

    Int_t mdcMods[6][4]= {
        {1,1,1,1},
        {1,1,1,1},
        {1,1,1,1},
        {1,1,1,1},
        {1,1,1,1},
        {1,1,1,1}
    };

    // recommendations from Vladimir+Olga
    // according to params from 28.04.2011
    Int_t nLayers[6][4] = {
        {6,6,5,6},
        {6,6,5,6},
        {6,6,5,6},
        {6,6,5,6},
        {6,6,5,6},
        {6,6,5,6}
    };
    Int_t nLevel[4] = {10,50000,10,5000};

    HDst::setupSpectrometer(beamtime,mdcMods,"rich,wall,mdc,tof,rpc,start,shower,tbox");

    HSpectrometer* spec = gHades->getSetup();
    Int_t fwdetMods[]   = {1,1,0,0,0,0,1,1,0}; // 1,1,0,0,1,0,1,0,0
    spec->addDetector(new HFwDetDetector);
    spec->getDetector("FwDet")->setModules(-1,fwdetMods);

    // beamtime mdcMods_apr12, mdcMods_full
    // Int_t mdcset[6][4] setup mdc. If not used put NULL (default).
    // if not NULL it will overwrite settings given by beamtime
    // detectors (default)= rich,mdc,tof,rpc,shower,wall,tbox,start

    HDst::setupParameterSources(paramSource,asciiParFile,rootParFile,paramrelease);  // now, APR12_gen2_dst
    //HDst::setupParameterSources("oracle",asciiParFile,rootParFile,"now"); // use to create param file
    // parsource = oracle,ascii,root (order matters)
    // if source is "ascii" a ascii param file has to provided
    // if source is "root" a root param file has to provided
    // The histDate paramter (default "now") is used wit the oracle source

    HDst::setDataSource(3,"",inFile,refId); // Int_t sourceType,TString inDir,TString inFile,Int_t refId, TString eventbuilder"
    // datasource 0 = hld, 1 = hldgrep 2 = hldremote, 3 root
    // like "lxhadeb02.gsi.de"  needed by dataosoure = 2
    // inputDir needed by dataosoure = 1,2
    // inputFile needed by dataosoure = 1,3


    if(kParamFile) {
        TDatime time;
        TString paramfilename= Form("allParam_JUL14_gen2_%02i%02i%i_%02i%02i%02i_refID_%i",time.GetDay(),time.GetMonth(),time.GetYear(),time.GetHour(),time.GetMinute(),time.GetSecond(),refId);  // without .root

        if(gSystem->AccessPathName(Form("%s.root",paramfilename.Data())) == 0){
            gSystem->Exec(Form("rm -f %s.root",paramfilename.Data()));
        }
        if(gSystem->AccessPathName(Form("%s.log",paramfilename.Data())) == 0){
            gSystem->Exec(Form("rm -f %s.log" ,paramfilename.Data()));
        }

        if (!rtdb->makeParamFile(Form("%s.root",paramfilename.Data()),Form("%ssim",beamtime.Data()),"01-JUN-2014","05-JUN-2014")) {
            delete gHades;
            exit(1);
        }
    }

    //--------------------------------------------------------------------
    // ----------- Build TASK SETS (using H***TaskSet::make) -------------
    HStartTaskSet        *startTaskSet        = new HStartTaskSet();
    HRichTaskSet         *richTaskSet         = new HRichTaskSet();
    HRpcTaskSet          *rpcTaskSet          = new HRpcTaskSet();
    //     HShowerTaskSet       *showerTaskSet       = new HShowerTaskSet();
    HTofTaskSet          *tofTaskSet          = new HTofTaskSet();
//     HWallTaskSet         *wallTaskSet         = new HWallTaskSet();
    HMdcTaskSet          *mdcTaskSet          = new HMdcTaskSet();
    HFwDetTaskSet        *fwdetTaskSet        = new HFwDetTaskSet();

    HTask *startTasks         = startTaskSet       ->make("simulation","");
    HTask *richTasks          = richTaskSet        ->make("simulation","");
    HTask *tofTasks           = tofTaskSet         ->make("simulation","");
//     HTask *wallTasks          = wallTaskSet        ->make("simulation");
    HTask *rpcTasks           = rpcTaskSet         ->make("simulation");
    //     HTask *showerTasks        = showerTaskSet      ->make("simulation","lowshowerefficiency");
    HTask *mdcTasks           = mdcTaskSet         ->make("rtdb","");
    HTask *fwdetTasks = fwdetTaskSet->make("simulation",
                                           "strawcal,rpccal,rpchitf,vf");

    //----------------SPLINE and RUNGE TACKING----------------------------------------
    HSplineTaskSet         *splineTaskSet       = new HSplineTaskSet("","");
    HTask *splineTasks     = splineTaskSet      ->make("","spline,runge");

    HParticleCandFiller    *pParticleCandFiller = new HParticleCandFiller   ("particlecandfiller","particlecandfiller"
    ,"NOMETAQANORM,NOMOMENTUMCORR,NOPATHLENGTHCORR");
    pParticleCandFiller->setFillMdc(kTRUE); // new : testing close pair
    HParticleTrackCleaner  *pParticleCleaner    = new HParticleTrackCleaner ("particlecleaner"   ,"particlecleaner");
    HParticleVertexFind    *pParticleVertexFind = new HParticleVertexFind   ("particlevertexfind","particlevertexfind","");
    HParticleEvtInfoFiller *pParticleEvtInfo    = new HParticleEvtInfoFiller("particleevtinfo"   ,"particleevtinfo",beamtime);
    HParticleBt            *pParticleBt         = new HParticleBt           ("RichBackTracking"  ,"RichBackTracking",beamtime);

    //----------------------- Quality Assessment -------------------------
    HQAMaker *qaMaker =0;
    if (!outDirQA.IsNull())
    {
        qaMaker = new HQAMaker("qamaker","qamaker");
        qaMaker->setOutputDir((Text_t *)outDirQA.Data());
        //qaMaker->setPSFileName((Text_t *)hldFile.Data());
        qaMaker->setUseSlowPar(kFALSE);
        qaMaker->setSamplingRate(1);
        qaMaker->setIntervalSize(50);
    }

    //------------------------ Master task set ---------------------------
    HTaskSet *masterTaskSet = gHades->getTaskSet("all");
    masterTaskSet->add(startTasks);

    masterTaskSet->add(tofTasks);
//     masterTaskSet->add(wallTasks);
    masterTaskSet->add(rpcTasks);
    masterTaskSet->add(richTasks);
    //     masterTaskSet->add(showerTasks);
    masterTaskSet->add(mdcTasks);
    masterTaskSet->add(fwdetTasks);
    masterTaskSet->add(splineTasks);
    masterTaskSet->add(pParticleCandFiller);
    masterTaskSet->add(pParticleCleaner);
    masterTaskSet->add(pParticleVertexFind); // run after track cleaning
    masterTaskSet->add(pParticleEvtInfo);
    masterTaskSet->add(pParticleBt);

    if (qaMaker) masterTaskSet->add(qaMaker);

    //--------------------------------------------------------------------
    //  special settings
    HMdcTrackDSet::setTrackParam(beamtime);
    if(!doMetaMatch)HMdcTrackDSet::setMetaMatchFlag(kFALSE,kFALSE);  //do not user meta match in clusterfinder
    if(doMetaMatchScale)HMetaMatchF2::setScaleCut(metaMatchScale,metaMatchScale,metaMatchScale); // (tof,rpc,shower) increase matching window, but do not change normalization of MetaQA

    HStart2HitFSim* starthitf = HStart2HitFSim::getHitFinder() ;
    if(starthitf) starthitf->setResolution(StartResolution);    // 60 ps start res

    //--------------------------------------------------------------------
    // find best initial params for segment fit (takes long!)
    if(doExtendedFit) {
        HMdcTrackDSet::setCalcInitialValue(1);  // -  1 : for all clusters 2 : for not fitted clusters
    }
    //--------------------------------------------------------------------

    //--------------------------------------------------------------------

    if (!gHades->init()){
        Error("init()","Hades did not initialize ... once again");
        exit(1);
    }

    //--------------------------------------------------------------------
    //----------------- Set not persistent categories --------------------
    HEvent *event = gHades->getCurrentEvent();

    HCategory *cat = ((HCategory *)event->getCategory(catRichCal));
    if(cat) cat->setPersistency(kTRUE);


    for(UInt_t i=0;i<sizeof(notPersistentCat)/sizeof(Cat_t);i++){
        cat = ((HCategory *)event->getCategory(notPersistentCat[i]));
        if(cat)cat->setPersistency(kFALSE);
    }
    //--------------------------------------------------------------------

    // output file
    gHades->setOutputFile((Text_t*)outFile.Data(),"RECREATE","Test",2);
    gHades->makeTree();

    Int_t nProcessed = gHades->eventLoop(nEvents,startEvt);
    printf("Events processed: %i\n",nProcessed);

    cout<<"--Input file      : "<<inFile  <<endl;
    cout<<"--QA directory is : "<<outDirQA<<endl;
    cout<<"--Output file is  : "<<outFile <<endl;

    printf("Real time: %f\n",timer.RealTime());
    printf("Cpu time: %f\n",timer.CpuTime());
    if (nProcessed) printf("Performance: %f s/ev\n",timer.CpuTime()/nProcessed);

    if(kParamFile) rtdb->saveOutput();

    delete gHades;
    timer.Stop();

    return 0;
}

#ifndef __CINT__
int main(int argc, char **argv)
{
    TROOT AnalysisDST("AnalysisDST","compiled analysisDST macros");

    TString nevents,startevent;
    switch (argc)
    {
        case 2:
            return analysisDST(TString(argv[1]), "dst"); // inputfile + "dst"
            break;
        case 3:
            return analysisDST(TString(argv[1]),TString(argv[2])); // inputfile + outdir
            break;
        case 4:  // inputfile + outdir + nevents
            nevents=argv[3];

            return analysisDST(TString(argv[1]),TString(argv[2]),nevents.Atoi());
            break;
            // inputfile + nevents + startevent
        case 5: // inputfile + outdir + nevents + startevent
            nevents   =argv[3];
            startevent=argv[4];
            return analysisDST(TString(argv[1]),TString(argv[2]),nevents.Atoi(),startevent.Atoi());
            break;
        default:
            cout<<"usage : "<<argv[0]<<" inputfile outputdir [nevents] [startevent]"<<endl;
            return 0;
    }
}
#endif

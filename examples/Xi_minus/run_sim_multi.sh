#!/bin/bash

# update this variable if hgeant is not in your $PATH
HGEANT_DIR=

if [ -z ${1} ]; then
    echo You need to pass filelist as an argument
    exit
fi

for ifile in $(cat $1); do

    # preapre name for temporary file
    tmp_file=/tmp/geaini_$(basename ${ifile} .evt).ini

    # update placeholders and generate temporary file
    sed \
        -e "s|@input@|./${ifile}|" \
        -e "s|@output@|$(basename ${ifile} .evt)_.root|" \
        geaini_multi.dat > ${tmp_file}

    # run simulation
    if [ -z ${HGEANT_DIR} ]; then
        hgeant -b -c -f ${tmp_file}
    else
        ${HGEANT_DIR}/hgeant -b -c -f ${tmp_file}
    fi

    # remove file
    rm -fv ${tmp_file}
done

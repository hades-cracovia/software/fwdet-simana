#!/bin/bash

# update this variable if hgeant is not in your $PATH
HGEANT_DIR=

if [ -z ${HGEANT_DIR} ]; then
    hgeant -b -c -f geaini_single.dat
else
    ${HGEANT_DIR}/hgeant -b -c -f geaini_single.dat
fi

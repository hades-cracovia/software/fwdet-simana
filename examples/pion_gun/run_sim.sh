#!/bin/bash

# update this variable if hgeant is not in your $PATH
HGEANT_DIR=

if [ -z ${HGEANT_DIR} ]; then
    hgeant -b -c -f geaini.dat
else
    ${HGEANT_DIR}/hgeant -b -c -f geaini.dat
fi

################################################################################
#
#  by Rafal Lalik
#
#  email: Rafal.Lalik@ph.tum.de
#
################################################################################
This README describes how to prepare examples for execution.

To avoid creating redundant files and working with the repository tracked files
(problem when commiting changes), examples should be used in another place and
appropriate files copied.


General informations
=============================

All directories in the commadns below are relative to the working directory
which is your repository main directory, e.g.

You are in your home dorectory and you fetched the repository:

   cd ~
   git clone gitolite@transfer.ktas.ph.tum.de:fwdet-simana.git

Your working directory is then:
  ~/fwdet-simana


Example preparation
=============================

1. Go to your working directory, e.g. (check above for explanation)

   cd ~/fwdet-simana

2. Create local directory, e.g.

   mkdir simul

3. Copy selected example there, e.g. for Xi_minus single file analysis

   cp -rp examples/Xi_minus simul/mytests

4. Copy or link missing files. Copying allows you to modify files without
   interacting with the tracked repository files, but all changes in the
   reference files (e.g. geometry) must by later updated manually. Linking
   allows you to have always files up-to-date after resyncing the repository.

   For copying method:

   cp -rp geom12500 fwdet_files simul/mytests

   For linking method:

   ln -s geom12500 simul/mytests/
   ln -s fwdet_files simul/mytests/

5. When your files are ready, go to the example directory, read its README file
   for more details, and execute it.

   cd simul/mytests
   cat README

6. Good luck! Write me back if something is wrong, you need help or you have
   new ideas.

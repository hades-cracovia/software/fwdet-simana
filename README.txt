********************************************************************************
Description:
Set of simple tools for FwDet simulations, analysis and verification

Date: 08.07.2016

Contact: Rafal Lalik <Rafal.Lalik@ph.tum.de>
********************************************************************************



1. Running and using simulations
================================================================================

A. Using 1-shot generator from geainifwdettest_kine90.dat

   Run using ./run_gean_kine90.sh
   Generates kine1.root output.

   Shoots 10k (one per event) pi- in theta=0-11, phi=0-90 at p=500:
   KINE 1 0. 11. 0. 90. 500.0 500.0 8.0 0. 0. 0.
   TRIG 10000

B. Geometry of the Straw tracking stations

   Straw Detector is made of two stations (modules) of four layers (cells) each.
   The physical cells are rotated by 0, 90, 0, 90 for the first station, and by
   0, 90, +45, -45 for the second. The same orientation is implemented for the
   simulation geometry. Since geant calculates hits in the local (cell)
   coordinate system, there is simple and natural correlation between x-position
   in the cell and the fired straw in the digitizer: straw ~ x.

C. Geometry display

   Can be viewed by running:

   root show_geom_in_root_T1.C
   root show_geom_in_root_T2.C

   for the first and the second straw station respectively.


2. Running the analysis
================================================================================

A. Compile the analysis code:

   cd analysis
   make # analysisDst will be installed in ..
   cd -

B. Run the analysis

   ./analysisDst kine1 # omit the file extension

   Will produce kine1_dst.root

   Interesting categories in the T tree are: 
    * HGeantFwDet -- kine simulations of the geant
    * HFwDetStrawCalSim -- digitizer output

3. Histograms
================================================================================

A. Compile the histograms code

   cd hists
   make # drawhists will be installed in ..
   cd -

B. Run the histograms tool

   ./drawhists kine1_dst,root

   Will produce output.root

   List of histograms and canvases:

   * h_gmodXcellY_xy -- x,y distribution for module X [0..1] and cell Y [0..3]
   * c_gmodX -- canvas plotting all cells for given X-module

   Since kine shots pions only in phi=0-90, it is expected to see that e.g.
   h_gmod0cell1_xy is rotated by 90 in respect to h_gmod0cell0_xy. The same is
   for other pairs of {module,cell} like {0,2},{0,3} and {1,0},{1,1}.
   The cell {1,2} is rotated by +45 deg in respect to {1,0}, and {1,3} by -45
   deg. Refer to 2.B. for details.

   * h_dmodXcellY_straw_x -- correlation between geant-x and digi-straw
   * c_dmodX -- canvas plotting above

   These histograms correlate X-coordinate of the hit in Geant Kine and straw
   number from the digitizer.
